<?php

namespace Igromafia\Game\Model;

use Bitrix\Main\Entity\DataManager,
    Bitrix\Main;

class VoteTable extends DataManager {

    public static function getTableName() {
        return 'b_askaron_ibvote_event';
    }

    public static function getMap() {
        $fieldsMap = array(
            new Main\Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
                    )),
            new Main\Entity\IntegerField('ELEMENT_ID'),
            new Main\Entity\IntegerField('ANSWER'),
            new Main\Entity\IntegerField('USER_ID'),
            new Main\Entity\IntegerField('STAT_SESSION_ID')
        );
        return $fieldsMap;
    }

}
