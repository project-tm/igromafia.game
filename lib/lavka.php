<?php

namespace Igromafia\Game;

use CIBlockElement,
    CIBlockFormatProperties,
    Project\Core\Utility;

class Lavka {

    static public function getGameList() {
        return Utility::useCache(array(__CLASS__, __FUNCTION__), function() {
                    $arResult = array();
                    $res = CIBlockElement::GetList(array(), array(
                                'ACTIVE' => 'Y',
                                'IBLOCK_ID' => Config::GAME_IBLOCK,
                                    ), false, false, array('ID', 'NAME'));
                    while ($arItem = $res->Fetch()) {
                        $arResult[] = trim($arItem['NAME']);
                    }
                    return $arResult;
                });
    }

    static public function getGame($name, $isProps = true) {
        static $cache = array();
        if (!isset($cache[$name])) {
            $cache[$name] = Utility::useCache(array(__CLASS__, __FUNCTION__, $name, $isProps), function() use($name, $isProps) {
                        $res = CIBlockElement::GetList(array(), array(
                                    'ACTIVE' => 'Y',
                                    'NAME' => $name,
                                    'IBLOCK_ID' => Config::GAME_IBLOCK,
                                        ), false, array('nTopCount' => 1), array('ID', 'DETAIL_PICTURE', 'DETAIL_PAGE_URL', 'PROPERTY_TRAILERS', 'PROPERTY_SYSTEMREQUIREMENTS'));
                        if ($obGame = $res->GetNextElement()) {
                            $arResult = $obGame->GetFields();
                            if ($isProps and $props = $obGame->GetProperties()) {
                                foreach ($props as $key => &$prop) {
                                    $prop = CIBlockFormatProperties::GetDisplayValue($arResult, $prop, "news_out");
                                    if (in_array($key, array(
//                        'SYSTEMREQUIREMENTS',
//                            'PLATFORM',
                                                'MULTIPLAYER',
                                                'DEVELOPERS',
                                                'PUBLISHER',
                                                'LOCALIZATION',
                                                'PUBLISHERLOCALIZATION',
                                                'DISTRIBUTIONMODEL',
                                                'OFFICIALSITE',
                                                'GENRES',
                                                    )
                                            )
                                    ) {
                                        $arResult['TABLE_PROPS'][$prop['NAME']] = $prop['DISPLAY_VALUE'];
                                        if ($prop['CODE'] == "OFFICIALSITE") {
                                            $arResult['TABLE_PROPS'][$prop['NAME']] = "<a target='_blank' href='" . $prop['VALUE'] . "'>" . $prop['VALUE'] . "</a>";
                                        } else if ($prop['CODE'] == "PLATFORM") {
                                            $platforms = array();
                                            foreach ($prop['VALUE'] as $value) {
                                                $name = Property::getPlatform($value);
                                                if ($name) {
                                                    $platforms[] = $name;
                                                }
                                            }
                                            $prop['VALUE'] = implode(' / ', $platforms);
                                        } else if ($prop['CODE'] == "GENRES") {
                                            $platforms = array();
                                            foreach ($prop['VALUE'] as $value) {
                                                $name = Property::getGenres($value);
                                                if ($name) {
                                                    $platforms[] = $name;
                                                }
                                            }
                                            $arResult['TABLE_PROPS'][$prop['NAME']] = implode(' / ', $platforms);
                                        }
                                    }
                                }
                                $arResult['PROPS'] = $props;
                            }
                            return $arResult;
                        } else {
                            return false;
                        }
                    });
        }
        return $cache[$name];
    }

}
