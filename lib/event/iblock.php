<?

namespace Igromafia\Game\Event;

use cUser,
    CIBlockElement,
    Igromafia\Game\Config;

class Iblock {

//    public static function OnBeforeIBlockElementAdd($arFields) {
////        preExit($arFields);
//    }

    public static function OnAfterIBlockElementAdd($arFields) {
        if (!empty($arFields['ID']) and ! empty($arFields['RESULT'])) {
            if (defined('GAME_IS_SOURSE')) {
                CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], GAME_IS_SOURSE, 'GAME');
            } elseif ($arFields['IBLOCK_ID'] == Config::DZHO_IBLOCK) {
                $arSelect = array('PROPERTY_SELLER');
                $arFilter = array(
                    'IBLOCK_ID' => $arFields['IBLOCK_ID'],
                    'ID' => $arFields['ID'],
                );
                $res = CIBlockElement::getList(array(), $arFilter, false, false, $arSelect);
                if ($arItem = $res->Fetch()) {
                    if (empty($arItem['PROPERTY_SELLER_VALUE'])) {
                        CIBlockElement::SetPropertyValues($arFields['ID'], $arFields['IBLOCK_ID'], cUser::GetId(), 'SELLER');
                    }
                }
            }
        }
    }

}
