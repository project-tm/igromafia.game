<?

namespace Igromafia\Game\Event;

use cUser;

class User {

    public static function OnBeforeProlog() {
        global $USER;
        if ($USER and $USER->IsAuthorized()) {
            CUser::SetLastActivityDate($USER->GetID());
        }
    }

}
